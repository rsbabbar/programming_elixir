defmodule ScratchTest do
  use ExUnit.Case
  doctest Scratch

  test "primes" do
    assert Scratch.primes(23) == [2, 3, 5, 7, 11, 13, 17, 19, 23]
  end

  test "calculate_totals" do
    tax_rates = [NC: 0.075, TX: 0.08]

    orders = [
      [id: 123, ship_to: :NC, net_amount: 100.00],
      [id: 124, ship_to: :OK, net_amount: 35.50],
      [id: 125, ship_to: :TX, net_amount: 24.00],
      [id: 126, ship_to: :TX, net_amount: 44.80],
      [id: 127, ship_to: :NC, net_amount: 25.00],
      [id: 128, ship_to: :MA, net_amount: 10.00],
      [id: 129, ship_to: :CA, net_amount: 22.00],
      [id: 130, ship_to: :NC, net_amount: 50.00]
    ]

    actual = Scratch.calculate_totals(orders, tax_rates)

    expected = [
      [id: 123, ship_to: :NC, net_amount: 100.00, total_amount: 107.50],
      [id: 124, ship_to: :OK, net_amount: 35.50, total_amount: 35.50],
      [id: 125, ship_to: :TX, net_amount: 24.00, total_amount: 25.92],
      [id: 126, ship_to: :TX, net_amount: 44.80, total_amount: 48.384],
      [id: 127, ship_to: :NC, net_amount: 25.00, total_amount: 26.875],
      [id: 128, ship_to: :MA, net_amount: 10.00, total_amount: 10.00],
      [id: 129, ship_to: :CA, net_amount: 22.00, total_amount: 22.00],
      [id: 130, ship_to: :NC, net_amount: 50.00, total_amount: 53.75]
    ]

    actual
    |> Enum.zip(expected)
    |> Enum.each(fn {a, b} -> assert Keyword.equal?(a, b) end)
  end
end
