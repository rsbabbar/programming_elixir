defmodule WeatherTest do
  use ExUnit.Case
  doctest WeatherHistory
  import WeatherHistory

  def loc_27 do
    [
      [1_366_225_622, 27, 15, 0.45],
      [1_366_229_222, 27, 17, 0.468],
      [1_366_232_822, 27, 21, 0.05]
    ]
  end

  def loc_28 do
    [
      [1_366_225_622, 28, 21, 0.25],
      [1_366_229_222, 28, 15, 0.60],
      [1_366_232_822, 28, 24, 0.03]
    ]
  end

  test "filter by default location" do
    assert for_location(test_data()) == loc_27()
  end

  test "filter by location" do
    assert for_location(test_data(), 28) == loc_28()
  end
end
