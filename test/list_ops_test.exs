defmodule ListOpsTest do
  use ExUnit.Case
  doctest ListOps
  @list [1, 2, 3]

  test "map" do
    assert ListOps.map(@list, fn x -> x + 1 end) == [2, 3, 4]
  end

  test "reduce" do
    assert ListOps.reduce(@list, 0, fn x, acc -> acc + x end) == 6
  end

  test "mapsum" do
    assert ListOps.mapsum(@list, 0, fn x -> x + 1 end) == 9
  end

  test "max" do
    assert ListOps.max([-4, 1, 0, 2, 3, 5, 10, 8, 9, 7, 6, 4]) == 10
  end

  test "max with negative numbers only" do
    assert ListOps.max([-10, -1, -3, -2]) == -1
  end

  test "span" do
    assert ListOps.span(1, 100) == Enum.to_list(1..100)
  end

  test "all?" do
    assert ListOps.all?(@list, fn x -> x < 10 end)
  end

  test "filter" do
    assert ListOps.filter(@list, fn x -> rem(x, 2) == 1 end) == [1, 3]
  end

  test "split" do
    assert ListOps.split(@list, 2) == {[1, 2], [3]}
  end

  test "take" do
    assert ListOps.take(@list, 1) == [1]
    assert ListOps.take(@list, 2) == [1, 2]
    assert ListOps.take(@list, 5) == @list
  end

  test "flatten" do
    assert ListOps.flatten(@list) == @list
    assert ListOps.flatten([1, [2, 3, [4]], 5, [[[6]]]]) == [1, 2, 3, 4, 5, 6]
  end
end
