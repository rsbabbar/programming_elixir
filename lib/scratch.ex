defmodule Scratch do
  @moduledoc """
  Scratch space for misc. exercises
  """

  @spec primes(number) :: [number]
  def primes(n)
  def primes(2), do: [2]
  def primes(3), do: [2, 3]
  def primes(n) when n < 2, do: []

  def primes(n) do
    for x <- 2..n, prime?(x), do: x
  end

  @spec prime?(number) :: boolean
  def prime?(x)
  def prime?(2), do: true
  def prime?(3), do: true
  def prime?(x) when x < 2, do: false

  def prime?(x) do
    Enum.all?(2..sqrt(x), fn num -> rem(x, num) != 0 end)
  end

  @spec sqrt(number) :: integer
  def sqrt(x), do: x |> :math.sqrt() |> trunc()

  @spec calculate_totals([Keyword.t()], Keyword.t()) :: [Keyword.t()]
  def calculate_totals(orders, tax_rates) do
    with_tax = fn amount, state ->
      amount + amount * Keyword.get(tax_rates, state, 0)
    end

    for order = [id: _, ship_to: state, net_amount: amount] <- orders,
        do: Keyword.put_new(order, :total_amount, with_tax.(amount, state))
  end
end
